package com.br.ufc.sts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecurityBootDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SecurityBootDemoApplication.class, args);
	}
}
